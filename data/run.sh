# estimated weights
nohup diffmer estimate GSE34128_cpgs_methyl_coverage_macaqueonly.bed --classfile macaque-samples.txt --aclass 34.83 --adiff 33.50 --scale 1.0 --slope 0.0018 --offset 0.054 > macaque-est.methrates &

# re-scaled weights
# nohup diffmer estimate GSE34128_cpgs_methyl_coverage_macaqueonly.bed --classfile macaque-samples.txt --aclass 34.83 --adiff 16.75 --scale 3.0 --slope 0.0018 --offset 0.054 > macaque-scaled.methrates &


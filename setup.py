# coding: utf-8

import sys

if sys.version_info < (3,4):
    print("At least Python 3.4 is required.", file=sys.stderr)
    exit(1)
try:
    from setuptools import setup
except ImportError:
    print("Please install setuptools before installing this package.", file=sys.stderr)
    exit(1)

# set __version__ and DESCRIPTION
exec(open("diffmer/version.py").read())

setup(
    name='diffmer',
    version=__version__,
    author='Nina Hesse and Sven Rahmann',
    author_email='nina.hesse@tu-dortmund.de, sven.rahmann@uni-due.de',
    description=DESCRIPTION,
    zip_safe=False,
    license='MIT',
    url='None',
    packages=['diffmer'],
    entry_points={
        "console_scripts":
            ["diffmer = diffmer.main:main",
            ]
        },
    package_data={'': ['*.css', '*.sh', '*.html']},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ]
)


import argparse
from importlib import import_module
from .version import __version__ as VERSION


def get_argument_parser():
    """
    return an ArgumentParser object p with this module's subcommands
    """
    p = argparse.ArgumentParser(
        description = "diffmer: differentially methylated regions",
        epilog="For research only.  Not suitable for diagnostic purposes."
        )
    # define subcommands: (command_name, help, parser_function, module_name, main_name)
    subcommands = [
        ("estimate", 
         "estimate CpG methylation levels in samples and classes",
         estimate_parser,
         "estimate", "main"),
        ("dmrs",
         "call differentially methylated CpGs and regions (DMRs) [new]",
         dmrs_parser,
         "newcaller", "main_dmrs"),
        ("call",
         "call differentially methylated CpGs and regions (DMRs) [old]",
         simple_call_parser,
         "caller", "main"),
        ("visualize",
            "compare data and measured values in DMRs by visualizing them",
            visualize_parser,
            "plots", "main"),
        ]
    # add global options here
    p.add_argument("--version", action="version", version=VERSION)
    # add subcommands to parser
    sps = p.add_subparsers()
    sps.required = True
    sps.dest = 'subcommand'
    for (name, help, f_parser, module, f_main) in subcommands:
        sp = sps.add_parser(name, help=help)
        sp.set_defaults(func=(module,f_main))
        f_parser(sp)
    return p

def visualize_parser(p):
    p.add_argument("input", metavar="PATH",
        help="BED or HDF5 file with numbers of methylated/unmethylated Cs for each CpG")
    p.add_argument("--output", metavar="PATH",
        help="path where to save the results")
    p.add_argument("--classfile", "-C", metavar="PATH",
        help="provide sample classes in a text file")
    p.add_argument("--classes", "-c", metavar="ASSIGNMENT", nargs="+",
        help="provide sample classes as SAMPLE1=0 SAMPLE2=0 SAMPLE3=1")
    p.add_argument("--classlevels", metavar="PATH",
        help="file with estimated class methylation levels")
    p.add_argument("--dmrs", metavar="PATH",
        help="file with DMRs to visualize")
    p.add_argument("--relative_positions", action="store_true",
        help="Plot only relative positions of CpGs")
    p.add_argument("--format", "-f", choices=("hdf5", "bed"),
        help="override auto-detected input format")
    p.add_argument("--manual_region",  metavar="ASSIGNMENT", nargs="+",
        help="provide regions manually as chr=chr1 start=0 stop=1")

def estimate_parser(p):
    p.add_argument("input", metavar="PATH",
        help="BED or HDF5 file with numbers of methylated/unmethylated Cs for each CpG")
    p.add_argument("--classfile", "-C", metavar="PATH",
        help="provide sample classes in a text file")
    p.add_argument("--classes", "-c", metavar="ASSIGNMENT", nargs="+",
        help="provide sample classes as SAMPLE1=0 SAMPLE2=0 SAMPLE3=1")
    p.add_argument("--precision", metavar="FLOAT", type=float, default=0.05,
        help="precision for methylation values")
    p.add_argument("--aclass", metavar="WEIGHT", type=float, default=17.82,
        help="weight alpha_class, within-class term [17.82]")
    p.add_argument("--adiff", metavar="WEIGHT", type=float, default=11.09,
        help="weight alpha_diff, between-class term [11.09]")
    p.add_argument("--slope", metavar="FLOAT", type=float, default=0.00224,
        help="slope of median methylation difference as a function of CpG distance [0.00224]")
    p.add_argument("--offset", metavar="FLOAT", type=float, default=0.033,
        help="offset of median methylation difference as a function of CpG distance [0.033]")
    p.add_argument("--scale", metavar="FLOAT", type=float, default=3.0,
        help="scale [3.0] for alpha_space = scale*ln(2)/(slope*distance+offset)")
    p.add_argument("--times", action="store_true",
        help="print progress and timing information to stderr")
    p.add_argument("--format", "-f", choices=("hdf5", "bed"),
        help="override auto-detected input format")
    p.add_argument("--cut", metavar="WEIGHT", type=float, default=0.1,
        help="Cuts problems where the weight of alpha_space drops below given weight [0.1]")


def simple_call_parser(p):
    p.add_argument("classlevels", metavar="PATH",
        help="file with estimated class methylation levels")
    p.add_argument("--delta", "-d", metavar="DELTA", type=float, default=0.3,
        help="methylation difference threshold for a DMC [0.3]")
    p.add_argument("--size", "--mindmcs", "-s", metavar="SIZE", type=int, default=1,
        help="minimum number of DMCs of a DMR [1]")
    p.add_argument("--meandelta", "-m", metavar="MEANDELTA", type=float, default=0.3,
        help="combine/cut DMRs such that each DMR subinterval of the given minimum size has an average methylation difference of at least this value")
    p.add_argument("--type",  choices=("AND", "OR"), default ="AND", 
        help="How to filter the results:Containing at least NoCpGs AND/OR diff methylation >= threshold ")


def dmrs_parser(p):
    p.add_argument("classlevels", metavar="PATH",
        help="file with estimated class methylation levels")
    p.add_argument("--delta", "-d", metavar="DELTA", type=float, default=0.1,
        help="methylation difference threshold for a DMC [0.3]")
    p.add_argument("--size", "-s", metavar="SIZE", type=int, default=1,
        help="minimum length (#CpGs) of a DMR [1]")
    p.add_argument("--meandelta", "-m", metavar="MEANDELTA", type=float, default=0.3,
        help="combine/cut DMRs such that each DMR subinterval of the given minimum size has an average methylation difference of at least this value")
    p.add_argument("--skip", "-p", metavar="SIZE", type=int, default=0,
        help="maximum number of consecutive non-DMCs in a DMR [0]")
    p.add_argument("--kind", metavar="KIND", default="any",
        choices=("hyper", "hypo", "both", "any"),
        help="find only hyper-/hypo-methylated regions, or both, or also mixed (any)")
    p.add_argument("--strict", action="store_true",
        help="strict reporting according to --kind (even if --skip>0)")


def main(args=None):
    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    (module, f_main) = pargs.func
    # import the appropriate module and call function f_main with pargs
    m = import_module("."+module, __package__)
    mymain = getattr(m, f_main)
    mymain(pargs)


if __name__ == "__main__":
    main()

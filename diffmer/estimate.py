# diffmer.estimate module

import sys
import os
import time
from itertools import chain
from functools import partial
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count
import numpy as np
import pandas as pd
from numba import jit

from . import io

@jit(nopython=True)
def minimize_weighted_absolute_and_squared_distance(w1, a1, w2, a2):
    """
    Input: Tupel aus Gewicht w1,2 und a1,a2 wobei w1,a1 fuer den Betrag sind
    min w1|a1-x|+w2(a2-x)^2
    return cost(x)
    w1 >= 0, w2 > 0!
    """
    #x_r falls x > a1
    x_r = a2 - (w1/(2*w2))
    if x_r >= a1:
        return w1*abs(x_r-a1) + w2* (x_r-a2)**2
    #x_l falls x < a1
    x_l = a2 + (w1/(2*w2))
    if x_l <= a1:
        return w1*abs(x_l-a1) + w2* (x_l-a2)**2

    #Es gibt kein globales Minimum, sondern es liegt an den Grenzen
    #Fall 1: x = a1
    f_x_1 = w2*(a1-a2)**2
    #Fall 2: x = a2
    f_x_2 = w1 * abs(a2-a1)
    if (f_x_1 < f_x_2):
        return  f_x_1
    else:
        return f_x_2


@jit(nopython=True)
def local_costs(localcosts, aclass, M, Cs, Ns):
    states = M.shape[0]
    samples = Ns.shape[0]
    localcosts[:] = 0
    for t in range(samples):
        N = Ns[t]
        C = Cs[t]
        ## assert N >= C
        if N > 0:  # keine Kosten fuer 0/0
            w2 = (0.0 + N*(N+2)**2) / (2*(C+1)*(N-C+1))
            for idx in range(states):
                m = M[idx]
                localcosts[idx] += minimize_weighted_absolute_and_squared_distance(aclass, m, w2, C/N)
    return localcosts


@jit(nopython=True)
def add_local_costs(this_V, M, adiff, aclass, Cs_case, Ns_case, Cs_control, Ns_control,
                    localcosts_case, localcosts_control):
    """add local costs to this_V"""
    states = M.shape[0]
    local_costs(localcosts_case, aclass, M , Cs_case, Ns_case)
    local_costs(localcosts_control, aclass, M , Cs_control, Ns_control)
    for ix in range(states):
        for iy in range(states):
            this_V[ix,iy] += localcosts_case[ix] + localcosts_control[iy] + adiff * abs(M[ix] - M[iy])


predecessor_dtype = np.dtype([('x', np.uint8), ('y', np.uint8), ('cost', np.float64)])
@jit(nopython=True)
def get_predecessors(last_V, aspace, predecessors):
    states = last_V.shape[0]
    # filter out entries which are greater than upper_bound_best_V
    upper_bound_best_V = last_V.min() + aspace * 2.0

    count = 0
    for jx in range(states):
        for jy in range(states):
            last_Vj = last_V[jx,jy]
            if last_Vj <= upper_bound_best_V:
                predecessors[count].x = jx
                predecessors[count].y = jy
                predecessors[count].cost = last_Vj
                count += 1
    return predecessors[:count]


@jit(nopython=True)
def add_space_costs(this_V, this_T, aspace, dspace, predecessors):
    """
    advance to next position, add space costs to this_V, and log traceback in this_T.
    """
    states = dspace.shape[0]

    predecessors = get_predecessors(this_V, aspace, predecessors)

    for ix in range(states):
        for iy in range(states):
            (best_jx, best_jy, best_V) = (0, 0, np.float64(np.inf))
            for pre in predecessors:
                (jx, jy) = (pre.x, pre.y)
                from_pre = pre.cost + aspace * (dspace[iy,jy] + dspace[ix,jx])
                if from_pre < best_V:
                    (best_jx, best_jy, best_V) = (jx, jy, from_pre)
            this_V[ix,iy] = best_V
            this_T[ix,iy,0] = best_jx
            this_T[ix,iy,1] = best_jy


@jit(nopython=True)
def do_traceback(classlevels, M, T, ix, iy):
    """
    compute traceback on T, starting at (-1,x,y).
    M contains the discrete methylation levels.
    classlevels is the Nx2 array to be written to
    """
    N = T.shape[0]+1
    classlevels[-1,0] = M[ix]
    classlevels[-1,1] = M[iy]
    for i in range(N-2, -1, -1):
        (ix, iy) = (T[i,ix,iy,0], T[i,ix,iy,1])
        classlevels[i,0] = M[ix]
        classlevels[i,1] = M[iy]


@jit(nopython=True, nogil=True)
def process_subproblem_(classlevels, a_class, a_diff, M, d_space,
                        a_space_table, C_case, N_case, C_control, N_control,
                        this_V, localcosts_case, localcosts_control, predecessors, traceback):
    states = M.shape[0]
    N = C_case.shape[0]

    add_local_costs(this_V, M, a_diff, a_class, C_case[0], N_case[0], C_control[0], N_control[0],
                    localcosts_case, localcosts_control)

    for n in range(1, N):
        add_space_costs(this_V, traceback[n-1], a_space_table[n-1], d_space, predecessors)
        add_local_costs(this_V, M, a_diff, a_class, C_case[n], N_case[n], C_control[n], N_control[n],
                        localcosts_case, localcosts_control)
    imin = this_V.argmin()
    ix = imin // states
    iy = imin %  states

    do_traceback(classlevels, M, traceback, ix, iy)


def process_subproblem(a_class, a_diff, M, d_space, subproblem):
    (chrom, pos, a_space_table, C_case, N_case, C_control, N_control) = subproblem

    states = M.shape[0]
    N = C_case.shape[0]

    this_V = np.zeros((states, states), dtype=np.float64)
    localcosts_case = np.empty((states,), dtype=np.float64)
    localcosts_control = np.empty((states,), dtype=np.float64)
    predecessors = np.empty(states * states, dtype=predecessor_dtype)
    traceback = np.empty((N-1, states, states, 2), dtype=np.uint8)
    classlevels = np.empty((N,2), dtype=np.float64)

    process_subproblem_(classlevels, a_class, a_diff, M, d_space,
                        a_space_table, C_case, N_case, C_control, N_control,
                        this_V, localcosts_case, localcosts_control, predecessors, traceback)
    return (chrom, pos, classlevels)


def precompute_a_space(positions, m, b, s_space):
    '''
    Given a numpy array 'positions',
    Return array a_space such that
    a_space[i] is the a_space weight between CpGs i and i+1
    '''
    distances = np.diff(positions)
    a_space = s_space * np.log(2) / (m*distances + b)
    return a_space


def separate_subproblems(chrom, data, a_space_weights, case_list, control_list):
    """
    yield for each subproblem (chrom, pos, a_space_table, C_case, N_case, C_control, N_control).
    The problem is separated where a_space drops below cut_weight, subproblems can be solved parallely
    a_space_weights: (a_space_slope, a_space_offset, a_space_scale, cut_weight)
    case_list, control_list: Lists of sample names
    eps: precision
    cut_weight: threshold for a_space
    """
    # get individual weights
    (slope, offset, scale, cut_weight) = a_space_weights
    pos = data.index.get_level_values('pos').values  # numpy array
    a_space_table = precompute_a_space(pos, slope, offset, scale)

    case = data[case_list]
    C_case = case.loc(axis=1)[:, "M"].values
    N_case = case.loc(axis=1)[:, "U+M"].values
    del case
    control = data[control_list]
    C_control = control.loc(axis=1)[:, "M"].values
    N_control = control.loc(axis=1)[:, "U+M"].values
    del control

    N = data.shape[0]  # number of CpGs
    segment_end_indices = np.where(a_space_table < cut_weight)[0] + 1
    segments = zip(chain((0,), segment_end_indices), chain(segment_end_indices, (N,)))

    for i, j in segments:
        yield (chrom, pos[i:j], a_space_table[i:j-1], C_case[i:j], N_case[i:j], C_control[i:j], N_control[i:j])


def get_subproblems(data, aspace_weights, cases, controls):
    chromosomes = sorted(set(data.index.get_level_values('chr')))
    # note: if chromosome awareness would be added to segmentation in separate_subproblems,
    # there wouldn't be any need to process by chromosome anymore.
    for chrom in chromosomes:
        # process cpgs in a single chromosome
        cdata = data.loc[chrom]
        yield from separate_subproblems(chrom, cdata, aspace_weights, cases, controls)


def process_data(data, weights, cases, controls, eps, timer):
    aclass, adiff, *aspace_weights = weights

    states = 1+int(round(1/eps))  # number of discrete methylation levels
    M = np.round(np.linspace(0.0, 1.0, states, dtype=np.float64), 8)  # set of discrete methylation levels
    assert M.shape[0] == states

    d_space = np.abs(M[:,np.newaxis] - M)
    _process_subproblem = partial(process_subproblem, aclass, adiff, M, d_space)

    print("chr\tpos\tcase\tcontrol")

    with ThreadPoolExecutor(cpu_count()) as executor:
        subproblems = get_subproblems(data, aspace_weights, cases, controls)
        for chrom, pos, classlevels in executor.map(_process_subproblem, subproblems):
            # set up and write DataFrame
            idx = pd.MultiIndex.from_product([[chrom], pos], names=["chr", "pos"])
            result = pd.DataFrame(classlevels, index=idx, columns=["case", "control"],)
            result.to_csv(sys.stdout, header=False, sep="\t")

            if timer is not None:
                timer.print("   {}: finished subproblem of size: {}".format(chrom, pos.shape[0]))


class Timer:
    """callabe timer that returns minutes since start"""
    def __init__(self, start=time.time()):
        self.start = start
    def __call__(self):
        return (time.time() - self.start) / 60.0
    def print(self, msg, file=sys.stderr):
        print("{:.3f}: {}".format(self(), msg), file=file)


def main(args):
    timer = Timer() if args.times else None
    if timer is not None:
        timer.print("read data")

    (samplenames, cases, controls) = io.get_samplenames(args.classes, args.classfile)
    data = io.read_input_file(args.input, samplenames, args.format)

    aspace_weights = (args.slope, args.offset, args.scale, args.cut)
    weights = (args.aclass, args.adiff) + aspace_weights
    # weights = [a_class, a_diff, slope_a_space_slope, offset_a_space, scale_a_space]
    # default: weights = [17.82, 11.09, 0.00224, 0.033, 3.0]
    # other default: weights= [34.83 , 16.75, 0.0018, 0.054, 3.0]
    # for 1 vs 1:  weights = [17.82, 0.5*11.09, 0.00224, 0.033, 2.0]

    if timer is not None:
        timer.print("process data")

    process_data(data, weights, cases, controls, args.precision, timer)

    if timer is not None:
        timer.print("done")

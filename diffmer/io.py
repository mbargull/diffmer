# diffmer.io module

import configparser

import numpy as np
import pandas as pd
import h5py



def read_bedfile(path, samplenames):
    """
    Read methylation information from a BED file.
    path: path to BED file
    samplenames: list of sample names, corresponding to order in the BED file

    Return a DataFrame, where:
    - each row corresponds to a CpG,
      (multi-indexed for chromosome and start position)
    - each pair of columns to a sample 
      (multi-indexed for sample and M, U+M).
    """
#    with open(path, 'rt') as f:
#        first_line = f.readline()
    typ = ["M", "U+M"]
#    names_read = first_line.split()

#    if set(names_read) != set(cases+controls):
#        raise ValueError("Sample names in BED file ({}) must correspond to case and control groups ({})".format(set(names_read),set(cases+controls)))
    multiind = pd.MultiIndex.from_product([samplenames, typ], names=["sample", "meth"])

    df = pd.read_csv(path, delim_whitespace=True, skiprows=0, header=None, names=multiind, index_col=[0,1,2], dtype={s:np.int32 for s in multiind})
    df.index.names = ["chr", "pos", "stop"]

    return df



def read_hdf5file(path, samplenames):
    """
    TODO: docstring
    """
    # TODO: refactor
    # TODO: force values to be int32
    typ = ["M", "U+M"]
    multiind = pd.MultiIndex.from_product([samplenames, typ], names=["sample", "meth"])
    df = pd.DataFrame(columns=multiind)
    f = h5py.File(path, "r")
    for chrom in f["positions"]:
        mvalues= np.array([])
        positions = f["positions"][chrom][:]
        index = pd.MultiIndex.from_product(["chr"+chrom, positions], names = ["chr", "pos"])
        for name in samplenames:

            values = f[name][chrom]["methylation"][:]

            if mvalues.size == 0:
                mvalues = values
            else:
                mvalues = np.vstack((mvalues, values))
        mvalues = np.transpose(mvalues)
        data = pd.DataFrame(mvalues,  index=index, columns = multiind)
        df = df.append(data)
    return df


def read_input_file(path, samplenames, format=None):
    """
    Read an input file with methylation information
    and return a DataFrame.
    
    The file must either have the ending .bed oder .hdf5, 
    or the format must be explictly specified.

    path: Path including input filename
    samplenames: list of sample names
    format: None or 'bed' or 'hdf5'
    """
    if len(samplenames) == 0:
        raise RuntimeError("no samples given; use --classfile or --classes")
    if format is None:
        if path.endswith(".bed"):
            format = "bed"
        elif path.endswith(".h5", ".hdf5"):
            format = "hdf5"
    if format=="bed":
        return read_bedfile(path, samplenames)
    elif format == "hdf5":
        return read_hdf5file(path, samplenames)
    else:
        raise ValueError("Unknown format in path '{}': '{}'".format(path,format))



def get_samplenames(classes, classfile):
    """
    Return a pair (cases, controls) of lists of sample names for case and control group.
    Assignment must be given by strings of the form samplename=class,
    where class is 0 (control) or 1 (case).
    
    classes: list of samplename=class strings, or None
    classfile: path to a file with assignments of this form, or None
    """
    if classes is None and classfile is None:
        return([], [], [])
    parser = configparser.ConfigParser(
        empty_lines_in_values=False, interpolation=None)
    parser.optionxform = str  # allow case-sensitive keys
    if classfile is not None:
        with open(classfile) as f:
            c = ["[classes]"] + f.readlines()
        parser.read_file(c)
    if classes is not None:
        parser.read_file(["[classes]"] + classes)
    samplenames = []
    cases = []
    controls = []
    for (samplename, cl) in parser.items("classes"):
        samplenames.append(samplename)
        if cl=="0":
            controls.append(samplename)
        elif cl=="1":
            cases.append(samplename)
        else:
            raise ValueError("Unknown class '{}' for sample '{}'".format(cl,samplename))
    return (samplenames, cases, controls)


def read_classlevels(path):
    """read estimated class methylation levels into a DataFrame and return it"""
    data = pd.read_csv(path, delim_whitespace=True, skiprows=1, header=None, names=["case", "control"],
            index_col=[0,1])
    data.index.names = ["chr", "pos"]
    return data





def read_dmrs(filename, manual_region):
    if filename is None and manual_region is None:
        raise ValueError("No regions to plot")
    if filename is not None:
        with open(filename, "r") as f:
            for line in f:
                chrom, start, stop, *_ = line.split("\t")
                yield chrom, int(start), int(stop)
    if manual_region is not None:
        parser = configparser.ConfigParser(
        empty_lines_in_values=False, interpolation=None)
        parser.read_file(["[region]"] + manual_region)
        yield parser["region"]["chr"], int(parser["region"]["start"]), int(parser["region"]["stop"])


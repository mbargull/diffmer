import os
from argparse import ArgumentParser

import numpy as np
import pandas as pd
import sys
from . import utilities



def filterCpGs(data, threshold, min_CpGs, type):
    """further filters differentially methylated Regions that have been clustered before.
    Results contain at least min_CpGs adjacent CpGs where each CpG as aIMD above the threshold
    threshold: Each DMC has an IMD of at least the threshold"""

    if type == "AND":
        data = data[abs(data.loc[:, "Mean_IMD"]) >= threshold ]
        data = data[abs(data.loc[:, "NoCpGs"]) >= min_CpGs]
    else:
        data_t = data[abs(data.loc[:, "Mean_IMD"]) >= threshold ]
        data_c = data[abs(data.loc[:, "NoCpGs"]) >= min_CpGs]
        data = data_c.append(data_t)
        data.drop_duplicates(inplace=True)
        data.sort(columns="start", inplace = True)
    return data




def clusterCpGs(frame, threshold, min_CpGs):
    """clusters differentially methylated Regions containing at least min_CpGs adjacent CpGs
    path_to_solution: It is assumed that there is a file results.bed at the given location
    containing the class methylation values for each position and class  
    threshold: Each DMC has an IMD of at least the threshold"""

    dmrs = pd.DataFrame()
    positions = frame.index.tolist()

    result = frame.iloc[:, 0] - frame.iloc[:, 1]

    hyper = pd.DataFrame(np.round(result[np.round(result,2) >= threshold],2))
    hypo = pd.DataFrame(np.round(result[np.round(result,2) <= -threshold],2))

    hypdmr = __cluster_DMR_single_chromosome(hyper, positions.copy(),  min_CpGs)
    if hypdmr is not None:
        dmrs = dmrs.append(hypdmr)

    hypodmr = __cluster_DMR_single_chromosome(hypo, positions,  min_CpGs)
    if hypodmr is not None:
        dmrs = dmrs.append(hypodmr)

    if not dmrs.empty:
        dmrs.sort("start", ascending = False, inplace = True)
    return dmrs
        


def __cluster_DMR_single_chromosome(result, all_indices, min_CpGs=4):
    """filters differentially methylated regions containing at least min_CpGs CpGs
    result: must be a dataframe containing the results
    all_indices: a List of all CpG-Positions
    min_Cpgs: The number of DMCs in a DMR"""
    res_list = []
    if result.empty:
        return
    akt_df = pd.DataFrame()
    #pprint(result.index)
    for i, j in utilities.pairwise(result.index):
        i_ind = all_indices.index(i)
        if all_indices.index(j) - i_ind == 1:
            #beide nebeneinander
            if akt_df.empty:

                akt_df = akt_df.append(result.loc[i])
            akt_df = akt_df.append(result.loc[j])
        else:
            #pruefe ob mehr als min_CpGs cpg
            if akt_df.shape[0] >= min_CpGs:
                res_list.append([akt_df.index[0], i, akt_df.shape[0], akt_df.mean(0).values[0]])
            akt_df = pd.DataFrame()
        if i_ind > 200:
                del all_indices[0:i_ind]
    #dmr am ende
    if akt_df.shape[0] >= min_CpGs:
        res_list.append([akt_df.index[0], j, akt_df.shape[0], akt_df.mean(0).values[0]])
    if not res_list:
        return

    dmrs = pd.DataFrame(res_list, columns=["start", "end", "NoCpGs", "Mean_IMD"])
    return dmrs





def call(data, chrname, args):
    (delta, size, meandelta) = (args.delta, args.size, args.meandelta)
    dmrs = clusterCpGs(data, delta, size)
    if not dmrs.empty:
        filtered_dmrs = filterCpGs(dmrs, meandelta, size, args.type)
        if not filtered_dmrs.empty:
            filtered_dmrs.index = [chrname for _ in filtered_dmrs.index]
            filtered_dmrs.to_csv(sys.stdout, header=False, sep= "\t")



def main(args):
    data = io.read_classlevels(args.classlevels)
    chromosomes = sorted(set(data.index.get_level_values('chr')))
    print("chr\tstart\tend\tNoCpGs\tMean_IMD")
    for chrom in chromosomes:
        # process CpGs in a single chromosome named chrom
        call(data.loc[chrom], chrom, args)
import sys
import os

import numpy as np
import pandas as pd
from numba import jit, uint32
from . import io




def properties(data):
    """return relevant information from the data as numpy arrays"""
    pos = data.index.values  # CpG positions as numpy array
    case = data["case"].values  # case methylation levels as numpy array
    control = data["control"].values  # dto for control
    diff = case - control  # signed differences (numpy array)
    return (pos, diff)


def get_dmcs(diff, delta, kind):
    """return Boolean array indicating DMCs according to delta and kind"""
    if delta < 0:
        raise RuntimeError("parameter delta must be non-negative")
    t = max(delta - 1E-8, 0.0)
    if kind == "any":
        dmcs = np.abs(diff) >= t
    elif kind == "hyper":
        dmcs = diff >= t
    elif kind == "hypo":
        dmcs = diff <= -t
    else:
        raise RuntimeError("unknown DMR kind '{}'".format(kind))
    return dmcs

 
def moving_average(x, windowsize, divide=True):
    """
    Return moving averages of x with a given windowsize.
    The length of the result is len(x)-windowsize+1.
    
    If not a moving average, but a moving sum is desired, use divide=False.
    """
    n = windowsize
    ret = np.cumsum(x, dtype=np.float64)
    ret[n:] = ret[n:] - ret[:-n]
    if divide:
        return ret[n-1:] / n
    return ret[n-1:]


@jit(nopython=True, locals=dict(start=uint32,l=uint32,v=uint32))
def find_runs(dmcs, runs):
    """
    Return number r of runs (sequences of consecutive 0s or 1s) in dmcs.
    Populate r x 3 array runs with (startindex, length, value) of each run.
    This array must have sufficient space to store all runs (e.g., n rows).
    """
    n = len(dmcs)
    if n == 0:  return 0
    v = dmcs[0]
    start = 0
    l = 1
    r = 0
    for i in range(1,n):
        vnew = dmcs[i]
        if v == vnew:
            l += 1
            continue
        runs[r,0]=start; runs[r,1]=l; runs[r,2]=v
        v = vnew
        start = i
        l = 1
        r += 1
    runs[r,0]=start; runs[r,1]=l; runs[r,2]=v
    return r+1


@jit(nopython=True)
def _adjust_dmcs(dmcs, skipruns):
    """Set dmcs[j]=1 for j in intervals described by skipruns."""
    for i in range(len(skipruns)):
        (start, length, v) = skipruns[i,:]
        end = start + length
        for j in range(start, end):
            dmcs[j] = 1
        

def call_dmrs(diff, args):
    """
    Call DMRs for CpGs with the given class methylation differences 'diff'.
    
    Return triple (dmrs, pos, diff), where
    - dmrs is an array of DMRs, each row is a pair (startindex, #CpGs)
    - pos is an array of CpG positions
    - diff is an array of signed methylation level differences (class - control)
    """
    if args.kind == "both":
        dmrs1 = filter_dmrs(diff, args.delta, "hyper", args.size, args.skip, args.meandelta)
        dmrs2 = filter_dmrs(diff, args.delta, "hypo", args.size, args.skip, args.meandelta)
        dmrs = np.vstack((dmrs1, dmrs2))
        permutation = np.lexsort((dmrs[:,1],dmrs[:,0]))
        dmrs = dmrs[permutation,:]
    else:
        dmrs = filter_dmrs(diff, args.delta, args.kind, args.size, args.skip, args.meandelta)
    return dmrs


def filter_dmrs(diff, delta, kind, size, skip, meandelta):
    dmcs = get_dmcs(diff, delta, kind)
    # compute runs in dmcs: array of (startindex, length, value [0/1])
    runs = np.zeros((len(dmcs),3), dtype=np.uint32)
    r = find_runs(dmcs,  runs)

    # allow to skip short zero runs: set dmc values to 1
    if skip > 0:
        rruns = runs[:r]
        toskip = (rruns[:,2] == 0) & (rruns[:,1] <= skip)
        _adjust_dmcs(dmcs, rruns[toskip,:])
    sizefilter = moving_average(dmcs, size, divide=False) >= size
    
    # compute moving averages of methylation differences
    if size > 1:
        averages = moving_average(np.abs(diff), size)
    else:
        averages = np.abs(diff)
    meanfilter = averages >= (meandelta - 1E-8)
    
    # combine size filter and meanfilter
    filter = sizefilter & meanfilter
    r = find_runs(filter, runs)
    dmrindex = runs[:r,2] == 1
    dmrs = (runs[:r,:2])[dmrindex]
    dmrs[:,1] += size - 1  # adjust length for window size

    # all done
    return dmrs


## output functions ###############################################################

def get_allowed_signs(kind, strict):
    """return tuple of allowed DRM signs (-1,0,1) to output"""
    if (not strict) or (kind=="any"):
        return (-1,0,1)
    if kind == "hyper":
        return (1,)
    if kind == "hypo":
        return (-1,)
    if kind == "both":
        return (-1,1)
    raise RuntimeError("unknown DMR kind '{}'".format(kind))


def output(dmrs, chrname, pos, diff, allowed_signs=((-1,0,1)), file=None):
    """
    dmrs: (#output x 2) array with rows (startindex, length)
    """
    # BED format: no header line, then one line per DMR:
    # chr  startpos  endpos  #CpGs   meandifference  sign  *differences
    if file is None:
        file = sys.stdout
    for row in range(dmrs.shape[0]):
        (startindex, length) = dmrs[row,:]
        endindex = startindex + length
        y = diff[startindex:endindex]
        meandiff = np.mean(np.abs(y))
        sign = int(np.all(y>=0)) - int(np.all(y<=0))
        if sign not in allowed_signs:  continue
        print(chrname, pos[startindex], pos[endindex-1], length, meandiff, sign,*y, sep="\t", file=file)


## main functions ###############################################################

def main_dmrs(args):
    main(args, call_dmrs)
    
def main(args, caller):
    data = io.read_classlevels(args.classlevels)
    allowed_signs = get_allowed_signs(args.kind, args.strict)
    chromosomes = sorted(set(data.index.get_level_values('chr')))
    #print("chr", "start", "end", "length", "meandiff", "sign","values", sep="\t", file=sys.stdout)
    for chrom in chromosomes:
        # process CpGs in a single chromosome named chrom
        cdata = data.loc[chrom]
        pos, diff = properties(cdata)
        dmrs = caller(diff, args)
        output(dmrs, chrom, pos, diff, allowed_signs)


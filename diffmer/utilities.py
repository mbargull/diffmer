import math
from itertools import tee
from pprint import pprint

import numpy as np
import h5py
import pandas as pd
import sys


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def cut(cut, df):
    """trennt nach Distanz auf und schreibt die Daten raus"""

    akt_pos = df.index[0]
    for x, y in pairwise(df.index):
        dist = y-x
        if dist >= cut:
            #nicht in so kleine teilstücke zerscheneiden..
            if(df.loc[akt_pos:x, :].shape[0] > 5000):
                #df.loc[akt_pos:x, :].to_csv(outfolder+str(akt_pos)+"_"+str(x)+".csv", sep= "\t")
                yield akt_pos, x
                akt_pos = y
    #am Ende angekommen
    yield akt_pos, x 


def compute_longest_piece(cut, df):
    """berechnet die Groesse des laengsten zusammenhaengenden Stuecks, wenn nach einer gegebenen bp-Distanz aufgetrennt wird
    cut: Distanz, nach der aufgetrennt wird
    df: Dataframe, der Index muss die cpg -positionen enthalten"""

    longest = 0
    akt_pos = df.index[0]

    for x, y in pairwise(df.index):
        dist = y-x
        if dist >= cut:
            if(df.loc[akt_pos:x, :].shape[0] > longest):
                longest = df.loc[akt_pos:x, :].shape[0]
            akt_pos = y        
    #am Ende angekommen
    if(df.loc[akt_pos:x, :].shape[0] > longest):
        longest = df.loc[akt_pos:y, :].shape[0]
    del df
    return longest


def compute_longest_pieces(data):
    zuordnung = dict()
    #distances = np.arange(100,210,10)
    distances = [1000]
    #initialisieren
    for d in distances:
        zuordnung[d] = 0

    for d in distances:
        a = compute_longest_piece(d, data)
        if a > zuordnung[d]:
            zuordnung[d] = a
            pprint(zuordnung)


def CpG_positions(chrom, folder):
    pos_f = h5py.File(folder+"index_hs37d5_Phix_Lambda.hdf5", "r")
    return pos_f[chrom]["cpg_positions"][:]


def read_melanome_methylation(chrom, folder):
    positions = CpG_positions(chrom, folder)

    f = h5py.File(folder+"19227_combi.hdf5", "r")
    values = f[chrom]["methylation"][:]

    methylated = np.sum(values[:, (0, 2)], axis=1)
    coverage = np.sum(values, axis=1)
    d = np.vstack((methylated, coverage))

    for filename in ["30505_combi.hdf5", "20780_combi.hdf5", "21437_combi.hdf5"]:
        f = h5py.File(folder+filename, "r")
        values = f[chrom]["methylation"][:]
        methylated = np.sum(values[:, (0, 2)], axis=1)
        coverage = np.sum(values, axis=1)
        d = np.vstack((d, methylated, coverage))

    data = pd.DataFrame(d, columns=positions, index=["M", "U+M", "M", "U+M", "M", "U+M", "M", "U+M"])
    return data


def melanome_methylation(folder, chrm=None):
    print("reading melanome methylation..")
    if chrm is None:
        for c in range(1, 23):
            chrom = str(c)
            frame = read_melanome_methylation(chrom, folder).transpose()
            case = frame.iloc[:, 0:4]
            control = frame.iloc[:, 4:8]
            yield frame, case, control, chrom
    else:
        print("single chromosome "+chrm)
        frame = read_melanome_methylation(chrm, folder).transpose()
        case = frame.iloc[:, 0:4]
        control = frame.iloc[:, 5:8]
        yield frame, case, control, chrm


def macaque_methylation(folder, chrm=None):
    print("reading macaque methylation..")
    if chrm is None:
        for c in range(1, 23):
            chrom = str(c)
            frame = read_macaque_methylation(chrom, folder)
            case = frame.iloc[:, [0, 1, 6, 7, 10, 11]]
            control = frame.iloc[:, [2, 3, 4, 5, 8, 9]]
            yield frame, case, control, chrom
    else:
        #print("single chromosome "+chrm)
        frame = read_macaque_methylation(chrm, folder)
        case = frame.iloc[:, [0, 1, 6, 7, 10, 11]]
        control = frame.iloc[:, [2, 3, 4, 5, 8, 9]]
        yield frame, case, control, chrm


def read_macaque_methylation(chrom, folder):
    frame = pd.read_csv(folder+"GSE34128_cpgs_methyl_coverage_macaqueonly.bed", delim_whitespace=True, header=None,  index_col=[0,1], usecols=[0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]).loc["chr"+chrom]
    frame.columns = ["M", "U+M", "M", "U+M", "M", "U+M", "M", "U+M", "M", "U+M", "M", "U+M"]
    return frame


def write_RadMeth_file(path_to_data, path_to_output):
    with open(path_to_output+"proportion_table2.txt", "a") as f:
        f.write("case_a\tcase_b\tcontrol_a\tcontrol_b\n" )
        for c in range(12,23):
            chrom = str(c)
            frame = read_melanome_methylation(chrom, path_to_data).transpose()
            frame.columns = np.arange(frame.shape[1])
            frame = frame[[1 ,0, 3, 2, 5, 4, 7, 6]]
            new_index = ["chr"+chrom+":"+str(pos)+":"+str(pos+1) for pos in frame.index]
            frame.index = new_index
            frame.to_csv(f, mode="a", header= False, sep = "\t")


def filter_BSmooth(path_to_data, path_to_output):
    data = pd.read_csv(path_to_data, delim_whitespace=True, index_col=None)
    #data = data.sort_index()
    data = data[[3, 4,5, 1, 0, 2]].sort(["Chr", "Start"])
    data.index = np.arange(len(data.index))
    data.to_csv(path_to_output+"sorted_dmr.csv", sep = "\t")


def rewritehdf5(folder="/home/hesse/Downloads/"):
    #positions = CpG_positions(chrom, folder)
    newf = h5py.File("aderhaut.hdf5", "w")


    for filename in ["30505_combi.hdf5", "20780_combi.hdf5", "21437_combi.hdf5", "19227_combi.hdf5"]:
        sample_name =  filename[:5]
        grp = newf.create_group(sample_name)

        f = h5py.File(folder+filename, "r")

        #for name in f:
        for c in range(1,23):
            chrom = str(c)
            a = grp.create_group(chrom)
            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((methylated, coverage))
            np.transpose(d)
            a.create_dataset("methylation", data=d)
    grp = newf.create_group("positions")
    for c in range(1,23):
        chrom = str(c)
        
        positions = CpG_positions(chrom, folder)
        grp.create_dataset(chrom, data=positions)


def converthdf5(folder):

    for c in range(1,23):
        chrom = str(c)
        for filename in ["30505_combi.hdf5"]:

            f = h5py.File(folder+filename, "r")

            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((methylated, coverage))
        for filename in ["19227_combi.hdf5", "20780_combi.hdf5", "21437_combi.hdf5"]:
            f = h5py.File(folder+filename, "r")
            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((d, methylated, coverage))
        d = d.transpose()
        pos_f = h5py.File(folder+"index_hs37d5_Phix_Lambda.hdf5", "r")
        positions = pos_f[chrom]["cpg_positions"][:]
        end_pos = positions+1
        index = [["chr"+chrom for _ in range(len(positions))], positions, end_pos]
        akt_df = pd.DataFrame(data = d, index= index)
        akt_df.to_csv(sys.stdout, sep = "\t", header = None)

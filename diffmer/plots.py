from . import io
from . import utilities
import os
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def compplot(rawdata, case_list, control_list,  classlevels, dmrs, out_folder,  show_relative , show_bounds = True):
    """visualizes a given region by colouring the methylation get_level_values
    rawdata: dataframe with methylated / unmethylated CpGs
    case_list: List of names for case samples
    control_list: list of names for control samples
    classlevels: computed class methylation levels, optional
    dmrs: the regions to plot, given as chromosom, start, stop
    out_folder: where to save the result, optional
    show_relative: show real genom positions of relative positions starting at 1
    show_bound: shows 2 extra positions at both sides as context"""
    if out_folder is not None:
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)

    for chromosom_name, l, r in dmrs:
        reg = str(l)+"_"+str(r)#string fuer plot
        df = rawdata.loc[chromosom_name]
        df.index = df.index.get_level_values('pos')

        if show_bounds:
            pos = df.index.tolist()
            l_minus_1 = pos.index(l)-2
            r_plus_1 = pos.index(r)+3
            df = df.iloc[l_minus_1:r_plus_1]
        else:
            df = df.loc[l:r]

        if show_relative is True:
            #reset index
            new_index = [1]
            for x, y in utilities.pairwise(df.index):
                new_index.append(y-x)
            new_index = np.cumsum(new_index)
            df.index = new_index
            reg = str(1)+"_"+str(new_index[len(new_index)-1])#string fuer plot

        plot_comparative(df,(case_list,control_list), chromosom_name+"_" + reg, out_folder)


        if classlevels is not None:#also show computed results
            result_df = classlevels.loc[chromosom_name]

            if show_bounds:
                l_minus_1 = result_df.index.tolist().index(l)-2
                r_plus_1 = result_df.index.tolist().index(r)+3
                result_df = result_df.iloc[l_minus_1:r_plus_1]
            else:
                result_df = result_df.loc[l:r]

            
            plot_comparative(df,(case_list,control_list), chromosom_name+"_"+reg, out_folder, show_results = True, results = result_df )





def set_alpha(N, i, j):
    val = N[i, j]
    if val > 20:
        val = 20
    alpha = val /20
    return alpha

def mycmap(x, N):
    #customized colormap. Alphachannel depends on Coverage
    colors = ["#4444dd", "#dd4444"]  # (blue -> red)
    fontcolor = lambda x: "#ffffff"
    mycolormap = mpl.colors.LinearSegmentedColormap.from_list("mycolormap", colors)

    tmp = mycolormap(x)
    #print(tmp)
    for i in range(tmp.shape[0]):
        for j in range(tmp.shape[1]):
            tmp[i,j][3] = set_alpha(N, i, j)
            #tmp[i,j][3] = 1.0
    return tmp

#########################################################
# comparative methylation plot

def plot_comparative(df,  samplenames, fname, out_folder, format="pdf", show_results = False, results = None):
    """
    Create and show/save a comparative methylation plot.
    df: DataFrame with the region to plot. Contains counts and a single index column
    samplenames: (case sample names, control sample names)
    fname: filename of the resulting image file
    out_folder: folder to write plot
    format: image format (e.g., 'png', 'pdf', 'svg')
    """
    #prepare data
    case_list = samplenames[0]
    case = df[case_list]

    C_case = case.loc(axis=1)[:, "M"].values.transpose()
    N_case = case.loc(axis=1)[:, "U+M"].values.transpose()

    control_list = samplenames[1]
    control = df[control_list]
    C_control = control.loc(axis=1)[:, "M"].values.transpose()
    N_control = control.loc(axis=1)[:, "U+M"].values.transpose()

    C = np.vstack((C_case, C_control))
    N = np.vstack((N_case, N_control))
    analysis = C / N

    case_rates = C_case/N_case
    control_rates = C_control/N_control
    mean_case = np.sum(C_case, axis=0)/np.sum(N_case, axis=0)
    mean_control = np.sum(C_control, axis = 0)/np.sum(N_control, axis=0)
    if show_results == True:
        assert results is not None
        result = np.array(results.as_matrix()).transpose()

        array = np.vstack((case_rates, mean_case, result, mean_control, control_rates ))
        coverage = np.vstack((N_case, np.sum(N_case, axis=0), np.sum(N_case, axis=0), np.sum(N_control, axis=0), np.sum(N_control, axis=0), N_control ))
    else:

        array = np.vstack((case_rates, mean_case, mean_control, control_rates))
        coverage = np.vstack((N_case, np.sum(N_case, axis=0), np.sum(N_control, axis=0),  N_control ))

    # determine cpg positions
    cpgpos = df.index
    if cpgpos is None:  return False  # inconsistent CpGs
    
    n,m = df.shape
    m = int(m/2)
    assert n is not None

    # initialize figure, set figure title/remark and axis title (subtitle)
    fig = plt.figure()
    titles = ["Comparison: " + fname]
    yheight = 0.8
    subtitles = ["{} samples, {} CpGs".format(m, n)]
    title = "\n".join(titles)
    subtitle = "\n".join(subtitles)
    fig.suptitle(title, fontsize=14, x=0.54)
    # if there is not enough space for labels at the left side,
    # increase the 'left' coordinate and reduce the 'width' in the following line
    ax = fig.add_axes([0.12, 0.1, 0.85, yheight]) # left, bottom, width, height    
    ax.set_title(subtitle, fontsize=12)

    # plot image
    image = ax.imshow(mycmap(array,coverage), 
        interpolation='none', origin='upper', vmin=0.0, vmax=1.0)
    ax.axhline(y=len(case_list)-0.5,  color='black')
    ax.axhline(y=len(case_list)+1.5,  color='black')
    if show_results == True:
        ax.axhline(y=len(case_list)+3.5,  color='black')

    ax.set_aspect('auto')
    xfontsize = 8 if n < 20 else 6
    #only show values for small dmrs
    if show_results == True and n<= 20:
        for i in range(m+4):
            for j in range(n):
                x = array[i,j]
                ax.text(j,i, "{:,.0%}".format(x), fontsize=xfontsize, color=fontcolor(x), ha='center')#Methylierung in %
                ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=xfontsize, color=fontcolor(x), ha='center')#Coverage
    else:
        if n<= 20:
            for i in range(m+2):
                for j in range(n):
                    x = array[i,j]
                    ax.text(j,i, "{:,.0%}".format(x), fontsize=xfontsize, color=fontcolor(x), ha='center')#Methylierung in %
                    ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=xfontsize, color=fontcolor(x), ha='center')#Coverage

    # column-wise methylation rates
    avgcolrates = np.mean(analysis, axis=0)
    x1labels = ["{:,.2f}".format(100*m) for m in avgcolrates]  
    x2labels = label_formatter(cpgpos)
    xlabels = [x1+"\n"+x2 for x1,x2 in zip(x1labels,x2labels)]    
    ax.set_xlabel('Average methylation level [%] / CpG-site')
    ax.set_xticks(range(n))
    ax.set_xticklabels(xlabels, fontsize=xfontsize)
    y1labels = case_list + ["mean_case", "mean_control"] + control_list
    y2labels = [ "(data)" for _ in range(len(y1labels))]
    if show_results == True:
        y1labels = y1labels[0:len(case_list)+1] + ["mu_i_case" , "mu_i_control" ] + y1labels[len(case_list)+1 : len(y1labels)]
        y2labels = y2labels[0:len(case_list)+1] + ["(computed)", "(computed)"] + y2labels[len(case_list)+1 : len(y1labels)]


    ylabels = [y1+"\n"+y2 for y1,y2 in zip(y1labels,y2labels)]
    ax.set_yticks(range(m+4)) if show_results else ax.set_yticks(range(m+2))
    yfontsize = 8 if m < 21 else 6
    ax.set_yticklabels(ylabels, fontsize=yfontsize)

    # save to file
    if out_folder is None:
        plt.show()
    else:
        if show_results == True:
            fig.savefig(out_folder+fname+"comp.pdf", format=format)  # bbox_inches="tight" cuts off title!
        else:
            fig.savefig(out_folder+fname+".pdf", format=format)  # bbox_inches="tight" cuts off title!
    plt.close(fig)
    fig = None



def label_formatter(positions, digits=3):
    """gets a list of positions and formats them"""
    labels = [".."+str(pos)[-digits:] if len(str(pos)) > 3 else str(pos)[-digits:] for pos in positions]
    return labels




def main(args):
    (samplenames, cases, controls) = io.get_samplenames(args.classes, args.classfile)
    data = io.read_input_file(args.input, samplenames, args.format)
    if args.classlevels is not None:
        classlevels = io.read_classlevels(args.classlevels)
    else:
        classlevels = None

    compplot(data, cases, controls, classlevels, io.read_dmrs(args.dmrs, args.manual_region), args.output, args.relative_positions, show_bounds=True)
